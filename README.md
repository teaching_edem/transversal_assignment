# MIP Assignment
This project contains the traversal assignment for the courses on Operations Research and statistics @ EDEM.
The overall problem to solve is to find the optimal distribution network for an online retail shop that operates in the city of Valencia, based on an historical database that contains the different orders in the web page. 

The assignment is divided into three parts:
## Part I: Data preparation
This part covers the preparation of the data for the other two parts and addresses primarily data Extraction, Transformation, and Load (ETL) using Pandas.
The original dataset can be downloaded from http://archive.ics.uci.edu/ml/datasets/Online+Retail and contains the transactions on an e-commerce 01/12/2010 and 09/12/2011 for a UK-based and registered non-store online retail.
The company mainly sells unique all-occasion gifts. Many customers of the company are wholesalers. 
The original dataset contains the following columns: 
- InvoiceNo: Invoice number. Nominal, a 6-digit integral number uniquely assigned to each transaction. If this code starts with letter 'c', it indicates a cancellation.
- StockCode: Product (item) code. Nominal, a 5-digit integral number uniquely assigned to each distinct product.
- Description: Product (item) name. Nominal.
- Quantity: The quantities of each product (item) per transaction. Numeric.
- InvoiceDate: Invoice Date and time. Numeric, the day and time when each transaction was generated.
- UnitPrice: Unit price. Numeric, Product price per unit in sterling.
- CustomerID: Customer number. Nominal, a 5-digit integral number uniquely assigned to each customer.
- Country: Country name. Nominal, the name of the country where each customer resides.

For the assignment, we have changed the CustomerID column name to a region id which represents a neighborhood in Valencia, as well as removed return orders and incorrect orders from the dataset.
The result is the dataset ecommerce_data.csv which is later used as input to the next part. 

To complete this part, you can plug in your solution to your Pandas Assignment. 

## Part II: Prediction
This part covers the prediction of the demand for the different regions for some planning periods, based on the dataset obtained in the part I. This assignment is part of the course in statistics.
To use it as input in the third part, you need to transform it into a file with the same columns as the as the e_commerce_data.csv dataset.

## Part III: Optimization
This part performs first calculates the distances from all source warehouses to intermedidate warehouses and from intermediate warehouses to the final warehouses. A MIP model then calculates the optimal distribution network for the planning period. 

# Requirements
You need a Python environment installed.

# Instructions
Clone with git:

```shell
git clone git@gitlab.com:teaching_edem/mip-assigment.git
```

Or alternatively Download and unzip.

Install the needed dependencies, listed in requirements, preferably using Conda. 

Fill in the parts corresponding to your assignments. For the Part I, you need to complete the code in the Part I/ETL/transform.py file. For the Part III, you need to enter your solution to the MIP modeling assignment in the Part III/Optimisation/model.py
