import pandas as pd
path_to_excel = 'Datasets/Online Retail.xlsx'
path_to_csv = 'Datasets/Online Retail_2.csv'


def read_excel_file():
    """
    Reads the e-commerce dataset in Datasets/Online Retail.xlsx and loads it into a data frame.

    Args:
        None

    Returns: Pandas dataframe containing data
    """
    return pd.read_excel(path_to_excel, sheet_name='Online Retail')

def read_file():
    """
    Reads the e-commerce dataset in Datasets/Online Retail.csv and loads it into a data frame.

    Args:
        None

    Returns: Pandas dataframe containing data
    """
    # WRITE YOUR CODE HERE


