from Part_I.ETL import read_file
import pandas as pd
import osmnx as ox
ecommerce_data = read_file()

VALID_QUERIES = pd.DataFrame([
    {"region_id": '01', "query": "Ciutat Vella, Valencia, Spain"},
    {"region_id": '02', "query": "Extramurs, Valencia, Spain"},
    {"region_id": '03', "query": "Ruzafa, Valencia, Spain"},
    {"region_id": '04', "query": "Gran via, Valencia, Spain"},
    {"region_id": '05', "query": "Campanar, Valencia, Spain"},
    {"region_id": '06', "query": "La saidia, Valencia, Spain"},
    {"region_id": '07', "query": "El pla del real, Valencia, Spain"},
    {"region_id": '08', "query": "L`olivereta, Valencia, Spain"},
    {"region_id": '09', "query": "Patraix, Valencia, Spain"},
    {"region_id": '10', "query": "Jesús, Valencia, Spain"},
    {"region_id": '11', "query": "Quatre Carreres, Valencia, Spain"},
    {"region_id": '12', "query": "El Cabanyal, Valencia, Spain"},
    {"region_id": '13', "query": "Cañamelar, Valencia, Spain"},
    {"region_id": '14', "query": "Camins al Grau, Valencia, Spain"},
    {"region_id": '15', "query": "Algirós, Valencia, Spain"},
    {"region_id": '17', "query": "Benimaclet, Valencia, Spain"},
    {"region_id": '18', "query": "Rascanya, Valencia, Spain"},
    {"region_id": '19', "query": "Benicalap, Valencia, Spain"},
    {"region_id": '20', "query": "Pobles del Nord, Valencia, Spain"},
    {"region_id": '21', "query": "Benimamet, Valencia, Spain"},
    {"region_id": '22', "query": "Beniferri, Valencia, Spain"},
    {"region_id": '23', "query": "Pobles del Sud, Valencia, Spain"}
    ]).set_index(['region_id'])

COUNTRY_MAP = {
    "United Kingdom": '01',
    "France": '02',
    "Australia": '03',
    "Netherlands": '04',
    "Germany": '05',
    "Norway": '06',
    "EIRE": '07',
    "Switzerland": '08',
    "Spain": '09',
    "Poland": '10',
    "Portugal": '11',
    "Italy": '12',
    "Belgium": '13',
    "Lithuania": '14',
    "Japan": '15',
    "Iceland": '17',
    "Channel Islands": '18',
    "Denmark": '19',
    "Cyprus": '20',
    "Sweden": '21',
    "Finland": '22',
    "Austria": '23',
    "Greece": '01',
    "Singapore": '02',
    "Lebanon": '09',
    "United Arab Emirates": '10',
    "Israel": '11',
    "Saudi Arabia": '12',
    "Czech Republic": '19',
    "Canada": '20',
    "Unspecified": '15',
    "Brazil": '13',
    "USA": '02',
    "European Community": '21',
    "Bahrain": '22',
    "Malta": '23',
    "RSA": '18'
     }


def get_demand(ec):
    """
    Reads the quantity and returns indexes where the value is greater than 0

    Args:
        ec - A Pandas Dataframe containing e-commerce records.

    Returns: a dataframe with the production site identifier
    """
    # Write your code here


def get_product_type(ec):
    """
    Reads the Stock Code and returns the first character of the string.
    This first character is used to identify the product type

    Args:
        ec - A Pandas Dataframe containing e-commerce records.

    Returns: a dataframe with the production site identifier
    """
    # Add a new column labeled 'Product type with the first character of the stock code
    ec['product_type'] = ec['StockCode'].str[0]

    # Filter values that do not start with a digit
    # Write your code here


def get_region(ec):
    """
    Reads the Country code and maps it to a region id

    Args:
        ec - A Pandas Dataframe containing e-commerce records.

    Returns: a dataframe with geo-referenced data
    """
    # Filter data without a valid CustomerID
    ec = ec.query('`CustomerID` > 0')

    # Map region using COUNTRY_MAP Dicticionary
    ec['region_id'] = ec['Country'].map(COUNTRY_MAP)
    return ec

def get_geodata():
    """
    Reads the region_id and returns the geo_data (Point geometry with latitude and longitude) using a valid query

    Args:
        None

    Returns: a dataframe with geo-referenced data
    """

    def get_centroid(query):
        # Get centroid from query
        # Centroid will have two coordinates
        # x: longitude, y: latitude
        geo_data = ox.geocode_to_gdf(query).centroid
        return geo_data

    gd = pd.DataFrame(index=VALID_QUERIES['query'].index)
    gd['centroids'] = VALID_QUERIES['query'].transform(get_centroid)
    #centroid = ec['region_id'].apply(get_centroid)
    gd['longitude'] = gd['centroids'].apply(lambda x: x.x)
    gd['latitude'] = gd['centroids'].apply(lambda x: x.y)

    return gd


def transform():

    # Filter values with incorrect quantities
    ec_demand = get_demand(ecommerce_data)

    # Set product type and filter values with incorrect product type
    ec_product_type = get_product_type(ec_demand)

    # Set demand region and filter values with incorrect postal codes
    ec_region = get_region(ec_product_type)


    # Set demand date
    ec_region['date'] = pd.to_datetime(ec_region['InvoiceDate']).dt.date
    # Add 9 years
    ec_region['date'] += pd.offsets.DateOffset(years=9)

    #Set indexes
    ec_geodata_indexed = ec_region.set_index(['InvoiceDate', 'date', 'StockCode', 'product_type', 'region_id'])

    # Group data by date product type and postal code to get sales
    ec_sales_grouped = ec_geodata_indexed.groupby(['date', 'product_type', 'region_id']).sum()

    ec_geodata = get_geodata()


    return (ec_sales_grouped, ec_geodata)






