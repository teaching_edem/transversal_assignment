from Part_I.ETL import transform


def load():
    (ecommerce_data_transformed, geo_data_transfomed) = transform()
    ecommerce_data_transformed.to_csv('Datasets/ecommerce_data.csv', columns=['Quantity'])
    geo_data_transfomed.to_csv('Datasets/geo_data.csv', columns=['longitude', 'latitude'])


