import networkx as nx
import osmnx as ox

# Get the map of Valencia
G_nx = ox.graph_from_place('Valencia, VC, Spain', network_type='drive')
# G_nx = ox.graph_from_address('46001 Valencia, VC, Spain', dist=1)

# convert osmids IDs to a list
osmids = list(G_nx.nodes)

# Relabel the nodes with integers. This facilitates handling the tree
G_nx = nx.relabel.convert_node_labels_to_integers(G_nx)

# give each node its original osmid as attribute, since we relabeled them
osmid_values = {k:v for k, v in zip(G_nx.nodes, osmids)}
nx.set_node_attributes(G_nx, osmid_values, 'osmid')


def get_distance(source, destination):
    """
    Given two coordinates (source and destination) calculate the shortest path between the closest nodes to the source and
    the closest node to the destination.
     A point is a tuple of two double values specifying latitude and longitude (eg point_1 = (39.464060144309364,
     -0.3624288516715025)).

    Args:
        source - A tuple containing latitude and longitude values of the source point.
        destination - A tuple containing latitude and longitude values of the source point.

    Returns: the minimum distance in meters (summation of great-circle distance between nodes in the shortest path)
    """
    # Get the nearest node to source
    node_1 = ox.get_nearest_node(G_nx, source)

    # Get the nearest node to destination
    node_2 = ox.get_nearest_node(G_nx, destination)

    # Get shortest path between source and destination nodes
    route = ox.shortest_path(G_nx, node_1, node_2)

    # Return the sum of the edge attribute length that corresponds to the length in meters
    return int(sum(ox.utils_graph.get_route_edge_attributes(G_nx, route, 'length')))

