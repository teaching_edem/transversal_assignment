import pandas as pd
path_to_geodata = 'Datasets/geo_data.csv'
path_to_ecommerce = 'Datasets/ecommerce_data.csv'


def read_geo_data_file():
    """
    Reads the regions dataset in Datasets/Online Retail.xlsx and loads it into a data frame.

    Args:
        None

    Returns: Pandas dataframe containing data
    """
    return pd.read_csv(path_to_geodata, dtype={'region_id': str})

def read_ecommerce_file():
    """
    Reads the e-commerce dataset in Datasets/Online Retail.csv and loads it into a data frame.

    Args:
        None

    Returns: Pandas dataframe containing data
    """
    return pd.read_csv(path_to_ecommerce, dtype={'region_id': str, 'product_type': str})


