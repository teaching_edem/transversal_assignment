from Part_III.ETL import read_ecommerce_file, read_geo_data_file, plants, warehouses
from Part_III.Routing import get_distance
import pgeocode
import pandas as pd

ecommerce_data = read_ecommerce_file()

geo_data = read_geo_data_file()


def init_plant_capacity():
    product_ids = ecommerce_data['product_type'].unique()
    plant_ids = plants['plant_id'].unique()
    capacities_idx = pd.MultiIndex.from_product([plant_ids, product_ids], names=['plants', 'products'])
    return  capacities_idx.to_frame()


def init_warehouse_operation_costs():
    product_ids = ecommerce_data['product_type'].unique()
    warehouse_ids = warehouses['warehouse_id'].unique()
    operation_costs_idx = pd.MultiIndex.from_product([warehouse_ids, product_ids], names=['warehouses', 'products'])
    return  operation_costs_idx.to_frame()


def get_yearly_demand():
    ecommerce_data['year'] = pd.to_datetime(ecommerce_data['date']).dt.year
    return ecommerce_data.set_index(['year', 'product_type']).groupby(['year', 'product_type']).sum()


def transform_demand():
    """
        Reads the ecommerce data and groups by month, product_type and region

        Args:
            None

        Returns: a dataframe with the demand
    """
    ecommerce_data['date'] = pd.to_datetime(ecommerce_data['date']).dt.to_period("M")
    ecommerce_data_index = ecommerce_data.set_index(['date', 'product_type', 'region_id'])
    #Group by period, product type and postal code
    ecommerce_data_grouped = ecommerce_data_index.groupby(['date', 'product_type', 'region_id']).sum()

    return pd.Series(ecommerce_data['date'].unique()), ecommerce_data_grouped


def get_distances(source_df, destination_df):

    def calculate_distance(row):

        return [get_distance(row['source'], row['destination'])]

    sources = source_df.index.values
    destinations = destination_df.index.values
    res_index = pd.MultiIndex.from_product([sources, destinations], names=[source_df.index.name, destination_df.index.name])
    coordinates = pd.DataFrame(0, index=res_index, columns=['dummy'])
    coordinates = source_df.join(coordinates, how="inner")
    coordinates = destination_df.join(coordinates, how="inner")
    result = coordinates.apply(calculate_distance, axis=1, result_type='expand')
    result.columns = ['distance']
    return result


def get_plant_warehouses():

    source = plants.set_index(keys=['plant_id'])
    source = source.rename(columns={'coordinates': 'source'})
    destination = warehouses.set_index(keys=['warehouse_id'])
    destination = destination.rename(columns={'coordinates': 'destination'})
    return get_distances(source, destination)


def get_warehouses_regions():
    source = warehouses.set_index(keys=['warehouse_id'])
    source = source.rename(columns={'coordinates': 'source'})
    destination = geo_data.set_index(keys=['region_id'])
    destination['destination'] = destination[['latitude', 'longitude']].apply(tuple, axis=1)
    warehouses_regions = get_distances(source, destination)
    return warehouses_regions


if __name__ == '__main__':
    warehouse_regions = get_warehouses_regions()
    plants_warehouses = get_plant_warehouses()

    






