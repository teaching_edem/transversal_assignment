from Part_III.ETL import warehouses, plants
from Part_III.ETL.transform import transform_demand, get_plant_warehouses, get_warehouses_regions, get_yearly_demand, \
    init_plant_capacity, init_warehouse_operation_costs

def init_data():
    production_capacities = init_plant_capacity()
    production_capacities.to_csv('Datasets/plant_capacities.csv', index=False)

    operation_costs = init_warehouse_operation_costs()
    operation_costs.to_csv('Datasets/operation_costs.csv', index=False)
    yearly_demand = get_yearly_demand()
    yearly_demand.to_csv('Datasets/yearly_demand.csv')



def load():
    periods, demand = transform_demand()
    transportation_costs_pw = get_plant_warehouses()
    transportation_costs_wr = get_warehouses_regions()

    periods.to_csv('Datasets/periods.csv')
    demand.to_csv('Datasets/demand.csv')
    transportation_costs_pw.to_csv('Datasets/costs_PW.csv')
    transportation_costs_wr.to_csv('Datasets/costs_WR.csv')
    warehouses.to_csv('Datasets/warehouses.csv', columns=['warehouse_id', 'building_cost', 'operation_cost'])
    plants.to_csv('Datasets/plants.csv', columns=['plant_id'])
