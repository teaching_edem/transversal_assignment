import pandas as pd
from Part_III.ETL.extract import *

warehouses = pd.DataFrame.from_dict([
    {"warehouse_id": 1, "coordinates": (39.45728723786173,  -0.3461905542352388),  "building_cost": 15,
     "operation_cost": 12},
    {"warehouse_id": 2, "coordinates": (39.4964300720693, -0.39517375910369273), "building_cost": 15,
     "operation_cost": 12},
    {"warehouse_id": 3, "coordinates": (39.4948598670054, -0.36278642308689907), "building_cost": 15,
     "operation_cost": 12},
    {"warehouse_id": 4, "coordinates": (39.45347869169637, -0.3555293613462748), "building_cost": 15,
     "operation_cost": 12},
    {"warehouse_id": 5, "coordinates": (39.46802653923393, -0.36980622585503453), "building_cost": 15,
     "operation_cost": 12},
    {"warehouse_id": 6, "coordinates": (39.467924448865524, -0.37562499868984967), "building_cost": 15,
     "operation_cost": 12},
    {"warehouse_id": 7, "coordinates": (39.487690395804165, -0.3969888379552108), "building_cost": 15,
     "operation_cost": 12},
    {"warehouse_id": 8, "coordinates": (39.490215424862996, -0.37763048461878007), "building_cost": 15,
     "operation_cost": 12}

])

plants = pd.DataFrame.from_dict([
    {"plant_id": 1, "coordinates": (39.46136270811244,  -0.3314705913601788)},
    {"plant_id": 2, "coordinates": (39.48139113773404, -0.3339024358414091)},
    {"plant_id": 3, "coordinates": (39.448274843453746, -0.3926636881865483)},
    {"plant_id": 4, "coordinates": (39.51149934646091, -0.4122195955728994)}
])





