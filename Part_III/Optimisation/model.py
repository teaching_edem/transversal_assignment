import pandas as pd
import pulp
import os

# Load data into dataframe
plants_df = pd.read_csv('Datasets/plants.csv')
demand_df = pd.read_csv('Datasets/demand.csv')
capacities_df = pd.read_csv('Datasets/plant_capacities.csv')
warehouses_df = pd.read_csv('Datasets/warehouses.csv')
costs_PW_df = pd.read_csv('Datasets/costs_PW.csv')
costs_WR_df = pd.read_csv('Datasets/costs_WR.csv')

plants = plants_df['plant_id'].unique()                  # This will be our index i
regions = demand_df['region_id'].unique()                 # This will be our index k
products = demand_df['product_type'].unique()             # This will be our index l
warehouses = warehouses_df['warehouse_id'].unique()       # This will be our index j
periods = demand_df['date'].unique()                      # This will be our index t

# Set the index in the dataframe and fecth the demand. In the expressions, we will use it as d[k, l, t]
demand_df_indexed = demand_df.set_index(['region_id', 'product_type', 'date'])
filled_demand_index = pd.MultiIndex.from_product([regions, products, periods], names=['regions', 'products', 'periods'])
filled_demand = pd.DataFrame(0, index=filled_demand_index, columns=['demand'])
filled_demand['demand'] = demand_df_indexed['Quantity']
d = filled_demand['demand'].fillna(0)

# Set the index in the dataframe and fetch the capacities. In the expressions, we will use the capacities as c[i,l]
capacities_df_indexed = capacities_df.set_index(['plants', 'products'])
c = capacities_df_indexed['capacity']

# Define the transportation costs from production plants to warehouses
# Set the index in the dataframe and fetch the distribution costs from production plants to warehouses,
# we will use it as a[i.j]
costs_PW_df_indexed = costs_PW_df.set_index(['plant_id', 'warehouse_id'])
a = costs_PW_df_indexed['distance']

# Define the transportation costs from warehouses to regions
# Set the index in the dataframe and fetch the distribution costs from production plants to warehouses,
# we will use it as b[j,k]
costs_WR_df_indexed = costs_WR_df.set_index(['warehouse_id', 'region_id'])
b = costs_WR_df_indexed['distance']


# A very large number of products
M = 99999999999999999

# Instantiate the model
model = pulp.LpProblem("Transport Planning", pulp.LpMinimize)

# binary { 1 if a warehouse j is built, 0 otherwise }
Y = pulp.LpVariable.dicts("Y",
                          [j for j in warehouses],
                          lowBound=0,
                          cat='Binary')


# units of product transported from plant i to warehouse j in period t
S = pulp.LpVariable.dicts("S",
                          [(i, j, l, t) for i in plants for j in warehouses for l in products for t in periods],
                          lowBound=0,
                          cat='Integer')

# units of product l transported from warehouse j to region k in period t
T = pulp.LpVariable.dicts("T",
                          [(j, k, l, t) for j in warehouses for k in regions for l in products for t in periods],
                          lowBound=0,
                          cat='Integer')
# Define the objective function. First we define to functions that help us define the objective function in a complex form

def transportation_costs_pw():
    """
        Returns the sum product of plants - warehouses distribution costs and decision variables

        Args:
            None

        Returns: An PuLP expression with the distribution costs from production plants to warehouses
        """
    return pulp.lpSum([
        a[i, j] * S[i, j, l, t]
        for i in plants for j in warehouses for l in products for t in periods])


def transportation_costs_wr():
    """
            Returns the sum product of warehouses - regions distribution costs and decision variables

            Args:
                None

            Returns: An PuLP expression with the distribution costs from warehouses to regions
            """
    # Write your code here



# from this functions, we define the objective function
model += transportation_costs_pw() + transportation_costs_wr(), "Distribution Costs"

# subject to:
# The production capacities must be met in all periods:
for i in plants:
    for l in products:
        for t in periods:
            model += pulp.lpSum([
                S[i, j, l, t]
            for j in warehouses]) <= c[i,l], " Capacity" + str((i, l, t))

# The demand for all products in all regions and in all periods must be satisfied

for k in regions:
    for l in products:
        for t in periods:
            # WRITE YOUR CODE HERE


# The amount of each product which arrives to a warehouse should be equal to the amount which exit from that warehouse
# in one period
for j in warehouses:
    for l in products:
        for t in periods:
            model += pulp.lpSum([
                S[i, j, l, t]
                for i in plants]) == pulp.lpSum([
                T[j, k, l, t]
                for k in regions]), "Flow " + str((j, l, t))


# A warehouse will supply a region only when the amount transported of all products in all periods from such warehouse to such region is nonzero
for j in warehouses:
        model += pulp.lpSum([
            T[j, k, l, t]
            for k in regions for l in products for t in periods]) <= M*Y[j], "Logic constraint warehouse " + str((j, k))


def solve():
    model.solve()
    return pulp.LpStatus[model.status], pulp.value(model.objective)


def load():
    Y_df = pd.DataFrame.from_dict(Y, orient="index",
                           columns=["Y"], dtype=object)
    Y_df["Solution"] = Y_df["Y"].apply(lambda item: item.varValue)



    S_df = pd.DataFrame.from_dict(S, orient="index",
                           columns=["S"], dtype=object)
    S_df["Solution"] = S_df["S"].apply(lambda item: item.varValue)
    s_idx = pd.MultiIndex.from_product([plants, warehouses, products, periods], names=['plant_id', 'warehouse_id', 'product_type', 'date'])
    S_df = S_df.reindex(s_idx)

    T_df = pd.DataFrame.from_dict(T, orient="index",
                                  columns=["T"], dtype=object)
    T_df["Solution"] = T_df["T"].apply(lambda item: item.varValue)
    t_idx = pd.MultiIndex.from_product([warehouses, regions, products, periods],  names=['warehouse_id', 'region_id', 'product_type', 'date'])
    T_df = T_df.reindex(t_idx)

    if os.path.exists('solution.xlsx'):
        try:
            os.remove('solution.xlsx')
        except:
            print("Close the solution file and try to solve again")

    with pd.ExcelWriter('solution.xlsx') as writer:
        Y_df.to_excel(writer, sheet_name='Y', index_label=['warehouses'])
        S_df.to_excel(writer, sheet_name="S", index_label=['plants', 'warehouses', 'products'])
        T_df.to_excel(writer, sheet_name="T", index_label=['warehouses', 'regions', 'products'])


if __name__=='__main__':
    status, objective = solve()
    load()

